//functions/hello.js
exports.handler = async (event, context) => {
    const { name = "Mirko" } = event.queryStringParameters;
    return {
      statusCode: 200,
      body: `Hello, ${name}`
    };
  };